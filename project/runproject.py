
from pj2utils import conservation, codon, restriction, addon

def generate_sequences():
	""" 
	generates list of N sequences where N is the number specified in param file
	-promoter, shine, start, stop, terminator
	-all lowercase except start/stop
	-shine exposed when effector present
	-aa conservation like protein.fasta
	-relative codon usage like codonfreq.txt
	-identical/3rd-differing codons far apart
	"""
	protein_file = 'inputs/protein.fasta'
	params_file = 'inputs/params.txt'
	codon_file = 'inputs/codonfreq.txt'
	rs_file = 'inputs/sites.fasta'

	# generates conserved sequences 
	try:
		f = open(params_file, 'r')
		N = int(f.readline().strip())
	except IOError, e:
		print e
	threshold = 1
	while threshold > .62:
		threshold, c, p = conservation.make_proteins(protein_file, N)
	conserved_seq = p

	# generates final dna sequences of conserved sequences 
	final_sequences = []
	freq_list = codon.read_codon(codon_file)
	percent_list = codon.relative_to_percentage(freq_list)
	sorted_list = codon.reverse_sort(percent_list)
	sorted_list_2 = codon.convert_freq(sorted_list, conserved_seq)
	sequences = codon.feed_protein(sorted_list_2, conserved_seq)

	# removes restrictoin sites 
	restriction.restriction_sites = restriction.parse_file(rs_file)
	rsites_removed = restriction.delete_rsites(sequences)
	for sequence in rsites_removed:
		split = list(sequence)
		for i in xrange(3):
			split[i] = split[i].upper()
			split[-i-1] = split[-i-1].upper()
		split = ''.join(split)
		final_sequences.append(split)
	return final_sequences


def generate_full(sequences):
	"""
	# adds promoter, terminator, and dna outside start and stop codon
	"""
	new = []
	effector_file = 'inputs/effector.fasta'
	effector_open = open(effector_file, 'r')
	effector = effector_open.readlines()[1]
	for sequence in sequences:
		new.append(addon.add_everything(sequence,effector))
	return new

def add_fasta(seq_list):
	"""
	generates output.fasta with fasta headers
	"""

	f = open('output/output.fasta', 'w')

	for i in range(len(seq_list)):
		entry = '>output' + str(i+1) + '\n'
		n = 50
		sequence = [seq_list[i][j:j+n] for j in range(0, len(seq_list[i]), n)]
		f.write(entry)
		for s in sequence:
			f.write(s + '\n')

if __name__ == '__main__':
	sequences = generate_sequences()
	final = generate_full(sequences)
	add_fasta(final)



