import random
	    
def conservation_stats(protein_file):
	"""
	arranges protein sequences by column to get conservation stats

	"""
	columns = list()
	infile = open(protein_file,'rb')
	i = 0
	lines = []
	
	for line in infile:
		if line.startswith('>'):
			lines[i:i+1] = ['']
			i += 1	
			seq = ''
		else:
			line = line.strip().upper()
			seq += line
			lines[i-1] = seq
	
	for j in range(len(lines[0])):
		letters = list()
		for i in range(len(lines)):
			letters.append(lines[i][j].strip())
		columns.append(''.join(letters))
	return columns, lines
	

def generate_protein(columns, N):
	"""
	generates random peptide based on the conservation stats of protein.fasta

	N: number of proteins to be generated as defined by the params.txt file 
	""" 
	peptides = list()
	peptide = ''
	for i in range(int(N)):
		peptide = ''
		for column in columns:
			peptide += random.choice(column)
		peptides.append(peptide)
	return peptides
	
def percent_identity(str1, str2):
	"""
	returns percent identity between two strings (float)
	"""
	count = 0
	for i in range(len(str1)):
		if str1[i] == str2[i]:
			count += 1
	return count/float(len(str1)), count
	
def redundancies(peptides, lines, columns):
	"""
	counts redundancies and regenerates peptides based on protein.fasta
	"""
	i = 0
	count = 0
	t = .8
	for peptide in peptides:
		while any(percent_identity(peptides[i], line)[0] > t for line in lines) and \
			any(percent_identity(peptides[i], pep)[0] > t for pep in peptides):
			peptides[i] = generate_protein(columns, 1)[0]
			count += 1
		i += 1
	return peptides, count

def entropy(peptides):
	"""
	Calculates average percent identity between pairs of output protein sequences. This
 	needs to be as low as possible.
	"""
	e = 0
	for peptide in peptides:
		for pep in peptides:
			e += percent_identity(peptide, pep)[0]
	return (e-len(peptides))/(len(peptides)*len(peptides))


def make_proteins(file, N):	
	"""main function to create protein sequences
	"""
	c,l = conservation_stats(file) # constant
	p = generate_protein(c,N) # initial result (could contain redundancies)
	p, count = redundancies(p,l,c) # removes redundancies
	e = entropy(p) # calculates "entropy"
	return e, count, p
	


