# Assembles restriction sites
import string
import re
from random import choice
import codon

def reverse_complement(string):
	"""
	returns the reverse_complement of input sequence
	"""
	rev = string[::-1]
	revcom = ''
	for base in rev:
		if base == 'a':
			revcom += 't'
		elif base == 't':
			revcom += 'a'
		elif base == 'g':
			revcom += 'c'
		elif base == 'c':
			revcom += 'g'		
	return revcom
	
def parse_file(file):
	"""
	parse through file and get restriction site information
	returns a list of restriction sites in file and their reverse complements
	"""
	infile = open(file)
	sites = []
	i = -1
	for line in infile:
		if line.startswith('>'):
			sites.append('');
			i += 1
		else:
			prev = sites[i]
			curr = prev + line.strip()
			sites[i] = curr.lower()
	for seq in sites:
		revcom = reverse_complement(seq)
		if revcom not in sites:
			sites.append(revcom)	
	return sites

def has_restriction_site(seq):
	"""
	checks to see if a single seq has restriction sites
	returns boolean True or False
	"""
	for rs in restriction_sites:
		pattern = re.compile(rs)
		match = pattern.search(seq.lower())
		if match != None:
			return True
		else:
			match = pattern.search(seq.upper())
			if match != None:
				return True
	return False


def delete_rsites(seqs):
	"""
	remove restriction sites from a list of sequences
	replaces restriction sites with synonymous codons
	"""
	for rs in restriction_sites:
		pattern = re.compile(rs)
		x = 0
		for s in seqs:
			i = 0
			while True:
				match = pattern.search(s.lower(), i)
				if match == None:
					match = pattern.search(s.upper(), i)
					if match == None:
						break
				start = match.start()
				i = match.end()
				s = alter_site(s.lower(), start, i)
			seqs[x] = s
			x += 1
	return seqs

def alter_site(seq, start, end):
	"""
	helper method for delete_rsites
	removes one restriction site in seq
	restriction site is at index start to end
	"""
	codon_start = start - (start % 3)
	cd = seq[codon_start:codon_start + 3]
	amino_acid = codon.codon_table()[cd]
	codons = codon.reverse_codon_table()[amino_acid]
	for c in codons:
		if c == cd:
			codons.remove(c)
	new = choice(codons)
	seq = seq[:codon_start] + new + seq[codon_start + 3:]
	return seq
	
restriction_sites = None
