from operator import itemgetter
from collections import Counter
import math
from copy import deepcopy

def codon_table():
	"""
	generate codon table in format:
	{amino acid: codon}
	"""
	bases = ['t', 'c', 'a', 'g']
	codons = [a + b + c for a in bases for b in bases for c in bases]
	amino_acids = 'FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG'
	codon_table = dict(zip(codons, amino_acids))
	return codon_table

def read_codon(filename):
	""" 
	parses through codonfreq.txt file and returns list
	with dictionary of codons (sorted by amino acid and 
	by third letter differences) and relative frequencies
	
	filename: file to be opened

	@return: frequency list
	"""
	
	#get file contents
	try: 
		with open(filename, 'r') as file:
			codons = []
			frequency = []
			for line in file:
				c,f = map(str,line.split())
				codons.append(c)
				frequency.append(float(f))
	except IOError, e:
		print e

	# find what each codon corresponds to
	c_table = codon_table()
	amino = [c_table[codon] for codon in codons]
	data = zip(amino, codons, frequency)

	# Sort first by the amino acids, and then 
	# alphabetically by the codons 
	freq_list = sorted(data, key=itemgetter(1))
	freq_list.sort(key=itemgetter(0))
	return freq_list

def relative_to_percentage(freq_list):
	"""
	Converts relative frequencies provided by codonfreq.txt into absolute percentages
	i.e. percentage: (residue relative frequency)/(sum of frequency of residues)

	freq_list: list to be edited

	@return: percentage list 
	"""
	percent_list = deepcopy(freq_list)
	checked_residues = []

	for f in freq_list:
		residue = f[0]
		if residue not in checked_residues:
			checked_residues.append(residue)
			# gets index of all matching residues in freq_list
			same_amino = [(i, freq.index(residue)) 
						for i, freq in enumerate(freq_list) if residue in freq]
			residue_sum = 0

			for same in same_amino:
				residue_sum += freq_list[same[0]][2]

			for line in same_amino:
				percent_list[line[0]] = [freq_list[line[0]][0], freq_list[line[0]][1],
									((freq_list[line[0]][2])/residue_sum)]
	return percent_list

def reverse_sort(freq_list):
	"""
	Takes freq_list and creates sublists based on amino acid. 
	Feeds sublist into reverse_sort_helper which dealphabatizes
	and adds to final sorted list

	freq_list: list to be reverse sorted 

	@return: reverse sorted list 
	"""
	checked_residues = []
	sorted_freq = []
	for f in freq_list:
		residue = f[0]
		if residue not in checked_residues:
			checked_residues.append(residue)
			same_amino = [(i, freq.index(residue)) 
								for i, freq in enumerate(freq_list) if residue in freq]
			sub_list = []
			for same in same_amino:
				sub_list.append(freq_list[same[0]])
			sorted_sub = reverse_sort_helper(sub_list, list(), True)
			sorted_freq.append(sorted_sub)
	sorted_flat = [item for sublist in sorted_freq for item in sublist]

	# Adding boolean marker to each codon
	for x in sorted_flat:
		x.insert(3, True)
	return sorted_flat

def reverse_sort_helper(old, new, top = True):
	"""
	recursively dealphabatizes, or reverse sorts list
	"""
	if top:
		new.append(old[0])
		old.pop(0)
	else:
		new.append(old[-1])
		old.pop(-1)
	if old:
		if top:
			return reverse_sort_helper(old, new, top = False)
		else:
			return reverse_sort_helper(old, new, top = True)
	else: 
		return new

def generate_protein(freq_list, protein_seq):
	""" 
	iterates through `dictionary` and creates protein dna sequence. 
	Marks true to false  if iterated through, and when all have been marked, converts 
	all back into true to be iterated through again.

	freq_list: list of codons and frequencies they may appear 
	protein_seq: the sequence to be created

	@return: DNA sequence corresponding to freq_list and protein_seq
	"""

	dna = []
	for residue in protein_seq:
		for line in freq_list:
			# if all have already been used up, markers are all false
			if true_counter(line[0], freq_list):
				same_amino = [(i, amino.index(residue)) 
					for i, amino in enumerate(freq_list) if residue in amino]
				for same in same_amino:
					freq_list[same[0]][-1] = True
			if residue == line[0] and line [2] > 0 and line[-1] == True:
				dna.append(line[1])
				line[2] -=1
				line[-1] = False
				residue = None
	return ''.join(dna)

def convert_freq(freq_list, conserved_seq):
	"""
	Multiplies percentage of codon frequencies to 
	actual frequencies in all of conserved_seq to generate
	absolute frequency list to be utilized through generating 
	"""
	amino_acids = ['*', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K',
					'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']
	amino = []
	amino_count = []

	seq_count = ''.join(conserved_seq)

	for a in amino_acids:
		amino.append(a) 
		amino_count.append(seq_count.count(a))

	zip_count = zip(amino, amino_count)
	# removes nonused amino acids
	total_count = [s for s in zip_count if s[1] > 0]
	# adjusts stop codons
	total_count.insert(0,('*', len(conserved_seq)))

	for i in xrange(len(freq_list)):
		for c in total_count:
			if freq_list[i][0] == c[0]:
				freq_list[i][2] = (math.ceil(freq_list[i][2]*c[1]))
	return freq_list

def feed_protein(freq_list, conserved_seqs):
	"""
	Feed proteins in one by one to generate dna

	freq_list: sorted frequency list
	conserved_seq: list of conserved proteins

	@return: list of generated dna sequences corresponding 
	to proteins in conserved_seq
	"""
	final_list = []
	for protein in conserved_seqs:
		protein = list(protein)
		protein.append('*')
		pro_dna = generate_protein(freq_list, protein)
		final_list.append(pro_dna)
	return final_list

def true_counter(residue, count_list):
	"""
	counts if all residues have been marked as False 

	residue: the residue to be counted 
	count_list: the list with the True/False markers to be evaluated 

	@return: True if all have been marked as False, False otherwise
	"""
	new_counter = []
	same_amino = [(i, count.index(residue)) 
					for i,count in enumerate(count_list) if residue in count]
	for same in same_amino:
		new_counter.append(count_list[same[0]])

	num_res = len(new_counter)
	num_true = Counter(x for sublist in new_counter for x in sublist)[False]

	if num_res is num_true:
		return True
	else:
		return  False


def reverse_codon_table():
	"""
	given an amino acid, will provide list of possible codons
	"""	
	reverse_codon_table = {
	'A': ['gct', 'gcc', 'gca', 'gcg'],
	'C': ['tgt', 'tgc'],
	'D': ['gat', 'gac'],
	'E': ['gaa', 'gag'],
	'F': ['ttt', 'ttc'],
	'G': ['ggt', 'ggc', 'gga', 'ggg'],
	'I': ['att', 'atc', 'ata'],
	'H': ['cat', 'cac'],
	'K': ['aaa', 'aag'],
	'L': ['tta', 'ttg', 'ctt', 'ctc', 'cta', 'ctg'],
	'M': ['atg'],
	'N': ['aat', 'aac'],
	'P': ['cct', 'ccc', 'cca', 'ccg'],
	'Q': ['caa', 'cag'],
	'R': ['cgt', 'cgc', 'cga', 'cgg', 'aga', 'agg'],
	'S': ['tct', 'tcc', 'tca', 'tcg', 'agt', 'agc'],
	'T': ['act', 'acc', 'aca', 'acg'],
	'V': ['gtt', 'gtc', 'gta', 'gtg'],
	'W': ['tgg'],
	'Y': ['tat', 'tac'],
	'*': ['TAA', 'TAG', 'TGA'],
	}
	return reverse_codon_table

def translate(seq):
	"""
	Converts DNA sequence into amino acids 
	"""
	seq = seq.lower().replace('\n', '').replace(' ', '')
	peptide = ''
	
	for i in xrange(0, len(seq), 3):
		codon = seq[i: i+3]
		amino_acid = codon_table().get(codon, '*')
		if amino_acid != '*':
			peptide += amino_acid
		else:
			break
	
	return peptide


