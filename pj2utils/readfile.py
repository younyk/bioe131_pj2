import sys
import subprocess

def rnafold_no_effector(dna):
	rnafoldout = open("noeffector.txt", "w")     # Opens an output file
	rnafold = subprocess.Popen(['RNAfold'], stdout=rnafoldout, stdin=subprocess.PIPE)  # Starts RNAfold
	rnafold.communicate(dna)[0]  #Inputs the first argument (input sequence) to the RNAfold prompt
	rnafoldout.close()

def rnafold_effector(dna,restraints):
	rnafoldout = open("yeseffector.txt", "w")     # Opens an output file
	rnafold = subprocess.Popen(['RNAfold', '-C',dna, restraints], stdout = rnafoldout,stdin=subprocess.PIPE)  # Starts RNAfold
	rnafold.communicate(dna +'\n' +restraints)[0]  #Inputs the first argument (input sequence) to the RNAfold prompt
	rnafoldout.close()
	
def read_rnafold(file):
	text = open(file,'r')
	lines = text.readlines()[1].strip()
	return lines

def is_good_no_effector(fold_structure,SDsite):
	fold = ''
	for site in SDsite:
		fold += fold_structure[site]	
	if fold == '......': 
		return False
	else:	
		return True	
		
def is_good_effector(fold_structure,SDsite):
	fold = ''
	for site in SDsite:
		fold+=fold_structure[site]
	if fold == '......':
		return True
	else:
		return False
		
def fold_correct(dna,effector):
	rnafold_no_effector(dna)
	a = 74 + 3*len(effector) 
	SDsite = range(a, (a+6))
	restraints = a*'.' + 6*'x' + (len(dna)-(a+6))*'.'
	rnafold_effector(dna,restraints)
	f_no_structure = read_rnafold('noeffector.txt')
	f_yes_structure = read_rnafold('yeseffector.txt')
	return is_good_no_effector(f_no_structure, SDsite) and is_good_effector(f_yes_structure, SDsite)	
		
