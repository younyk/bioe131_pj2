import random 
import restriction
import readfile

def make_effector_dna(effector): # converts effector from RNA to DNA
	newEffector = ''
	for base in effector:
		if base == 't':
			newEffector += 't'
		if base == 'u':
			newEffector += 't'
		if base == 'g':
			newEffector += 'g'	
		if base == 'c':
			newEffector += 'c'	
		if base == 'a':
			newEffector += 'a'
	return newEffector		
	
def gen_random_base(nbases): #generates sequence of n length with random bases
	string = ''
	for base in range(nbases):
		string += random.choice('a'+'t'+'g'+'c')	
	return string	

def gen_random_base2(nbases): #generates sequence of n length with random bases but no Cs
	string = ''
	for base in range(nbases):
		string += random.choice('ag')
	return string	

def gen_cg_rich_base(nbases):#generates a cg rich sequence of n length with random bases
	string = ''
	for base in range(nbases):
		string += random.choice(10*'c' + 10*'g' + 'a' +'t')
	return string		
	
def insert_riboswitch(effector, dnaseq):
	'''
	inserts in a riboswitch based off of the effector to a dna sequence as well as a
	shine delgarno. Returns the resulting DNA sequence.
	'''
	stem = gen_random_base(4) + effector[1:] 
	revComStem = restriction.reverse_complement(effector[1:])
	revComEffector = restriction.reverse_complement(effector)
	while stem == effector:
		stem = gen_random_base(4) + effector[1:] 
	loop = 't' + revComEffector + revComStem + stem + 'aggagg' + gen_random_base2(7) + dnaseq	
	return loop

def insert_terminator(dnaseq):
	'''
	Inserts a terminator to a dna sequence that already has a riboswitch added. Outputs 
	the whole returned DNA seq.
	'''
	cgRichSeq = gen_cg_rich_base(12)
	cgRichSeqRC = restriction.reverse_complement(cgRichSeq)
	seqtot= dnaseq + cgRichSeq + gen_random_base(5) + cgRichSeqRC + 'tttttttttt'
	return seqtot
	
def insert_promoter(dnaseq): #inserts promoters
	string = 'ttgaca' + gen_random_base2(19) + 'tataat' + gen_random_base(4) +dnaseq
	return string	

def add_everything(dnaseq, effector): # combines all functions into one
	DNAeffector = make_effector_dna(effector)
	dna = insert_promoter(insert_terminator(insert_riboswitch(DNAeffector,dnaseq)))
	while not readfile.fold_correct(dna, effector) and restriction.has_restriction_site(dna):
		dna = insert_promoter(insert_terminator(insert_riboswitch(DNAeffector,dnaseq)))	
	return dna
